# ROCK PAPER SCISSORS NOKIA

## Overview
**ROCK PAPER SCISSORS NOKIA** is a multithreaded, client-server C++ application utilizing socket programming on Windows. In this game, clients play Rock-Paper-Scissors with the server.

## Features
- Multithreaded architecture for handling multiple clients simultaneously.
- Each client operates on a separate thread.
- Server handles each client connection on a new detached thread.
- High scores are maintained and persisted in a file.
- Thread safety is ensured using `std::lock_guard` and `std::mutex`.

## How It Works

### Server Initialization:
- The server starts on the main thread and waits for client connections.

### Client Connection:
- Clients are created and each runs on a separate thread.
- When a client connects to the server, a new detached thread is spawned to handle communication with that client.

### Client-Server Interaction:
1. The client sends its name and ID to the server.
2. The server returns the last high score for the client from `HighScores.txt`.
3. The game of Rock-Paper-Scissors begins:
    - The client sends its choice (randomly generated) to the server.
    - The server generates its own choice and sends the result back to the client.
4. If the client wins:
    - The client sends the updated score to the server, which updates the high score.
5. If the client loses:
    - The client displays the score and exits.

### High Score Management:
- The server maintains a high score map.
- High scores are saved in `HighScores.txt`.
- After a client loses, the server displays the top 10 high scores.

## Thread Safety
- The program ensures thread safety using `std::lock_guard` and `std::mutex`.
- This guarantees that shared resources like High Scores are accessed in a safe manner, preventing data races and ensuring the integrity.

## Testing
- The project includes unit tests implemented with Google Test to ensure the correctness of utility functions and components.
- Unit tests are located in the test/ directory.

## File Descriptions
- **src/client/**: Contains the client-side code.
    - `Client.cpp`, `Client.h`: Client class implementation.
    - `Communication.cpp`, `Communication.h`: Handles communication between client and server.
- **src/server/**: Contains the server-side code.
    - `HandleClient.cpp`, `HandleClient.h`: Manages individual client connections.
    - `HighScoreManager.cpp`, `HighScoreManager.h`: Manages high scores.
    - `Server.cpp`, `Server.h`: Server class implementation.
- **src/Utils/**: Utility functions.
    - `GameUtil.cpp`, `GameUtil.h`: Game-related utilities.
    - `SocketUtil.cpp`, `SocketUtil.h`: Socket-related utilities.
- **test/**: Contains the unit tests for the program
    - `main.cpp`: Entry point for Google tests.
    - `GameUtilTest.cpp`: Tests the GameUtil class
- **main.cpp**: Entry point of the application.
- **CMakeLists.txt**: CMake build configuration.
- **HighScores.txt**: File to persist high scores.
- **README.md**: Project documentation.

## Project Structure

```plaintext
RockPaperScissorsNokia/
├── cmake-build-debug/
├── src/
│   ├── client/
│   │   ├── Client.cpp
│   │   ├── Client.h
│   │   ├── Communication.cpp
│   │   ├── Communication.h
│   ├── server/
│   │   ├── HandleClient.cpp
│   │   ├── HandleClient.h
│   │   ├── HighScoreManager.cpp
│   │   ├── HighScoreManager.h
│   │   ├── Server.cpp
│   │   ├── Server.h
│   ├── Utils/
│   │   ├── GameUtil.cpp
│   │   ├── GameUtil.h
│   │   ├── SocketUtil.cpp
│   │   ├── SocketUtil.h
│   ├── main.cpp                <-- Entry point for Program
├── test/                       
│   ├── main.cpp                <-- Entry point for Google Test
│   ├── GameUtilTest.cpp     
├── CMakeLists.txt
├── HighScores.txt
├── README.md
```
## Author
- Maria Sher
- Email: mariasher001@gmail.com