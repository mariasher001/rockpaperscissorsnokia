//
// Created by maria on 7/8/2024.
//

#include <gtest/gtest.h>

/*
 * Less tests created due to shortage of time
 */
int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}