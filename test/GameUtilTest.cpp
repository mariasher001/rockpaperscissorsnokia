//
// Created by maria on 7/8/2024.
//
#include <gtest/gtest.h>
#include "../src/Utils/GameUtil.h"


TEST(GameUtilTest, EnumToStringTest) {
    EXPECT_EQ(GameUtil::enumToString(RockPaperScissorsEnum::ROCK), "ROCK");
    EXPECT_EQ(GameUtil::enumToString(RockPaperScissorsEnum::PAPER), "PAPER");
    EXPECT_EQ(GameUtil::enumToString(RockPaperScissorsEnum::SCISSORS), "SCISSORS");
}

TEST(GameUtilTest, StringToEnumTest) {
    EXPECT_EQ(GameUtil::stringToEnum("ROCK"), RockPaperScissorsEnum::ROCK);
    EXPECT_EQ(GameUtil::stringToEnum("PAPER"), RockPaperScissorsEnum::PAPER);
    EXPECT_EQ(GameUtil::stringToEnum("SCISSORS"), RockPaperScissorsEnum::SCISSORS);
}

TEST(GameUtilTest, RandomChoiceTest) {
    RockPaperScissorsEnum choice = GameUtil::getRandomChoice();
    EXPECT_TRUE(choice == RockPaperScissorsEnum::ROCK ||
                choice == RockPaperScissorsEnum::PAPER ||
                choice == RockPaperScissorsEnum::SCISSORS);

}

TEST(GameUtilTest, GetResultTest) {
    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::ROCK, RockPaperScissorsEnum::ROCK), "DRAW");
    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::PAPER, RockPaperScissorsEnum::PAPER), "DRAW");
    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::SCISSORS, RockPaperScissorsEnum::SCISSORS), "DRAW");

    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::ROCK, RockPaperScissorsEnum::SCISSORS), "WIN");
    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::SCISSORS, RockPaperScissorsEnum::PAPER), "WIN");
    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::PAPER, RockPaperScissorsEnum::ROCK), "WIN");

    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::SCISSORS, RockPaperScissorsEnum::ROCK), "LOSE");
    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::PAPER, RockPaperScissorsEnum::SCISSORS), "LOSE");
    EXPECT_EQ(GameUtil::getResult(RockPaperScissorsEnum::ROCK, RockPaperScissorsEnum::PAPER), "LOSE");
}