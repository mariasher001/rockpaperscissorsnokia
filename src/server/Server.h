//
// Created by maria on 7/5/2024.
//

#ifndef ROCKPAPERSCISSORSNOKIA_SERVER_H
#define ROCKPAPERSCISSORSNOKIA_SERVER_H

#include <iostream>
#include "../Utils/SocketUtil.h"
#include <winsock2.h>
#include <thread>
#include <mutex>
#include "HandleClient.h"

class Server {
private:
    int port;
    char* ip;
    SOCKET listenerSocket;
    sockaddr_in ipPortService;
    std::mutex serverLogMutex;
    void bindSocketToIpPortService();
    void listenOnSocket();
    void acceptConnection();

public:
    Server(char *ip,int port);
    void startServer();
};

#endif //ROCKPAPERSCISSORSNOKIA_SERVER_H
