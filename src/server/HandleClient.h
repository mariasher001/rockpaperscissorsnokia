//
// Created by maria on 7/7/2024.
//
#ifndef ROCKPAPERSCISSORSNOKIA_HANDLECLIENT_H
#define ROCKPAPERSCISSORSNOKIA_HANDLECLIENT_H

#include <winsock2.h>
#include <mutex>
#include <iostream>
#include "../Utils/GameUtil.h"
#include "HighScoreManager.h"

class HandleClient {
private:
    static std::mutex handleClientLogMutex;
    static int receiveClientDetails(SOCKET socketConnectedToClient, int &clientId, std::string &clientName);
    static void sendHighScore(SOCKET socketConnectedToClient, int &clientId, std::string &clientName);
    static void startGame(SOCKET socketConnectedToClient, int &clientId, std::string &clientName);
public:
    static void startCommunication(SOCKET socketConnectedToClient);
};

#endif //ROCKPAPERSCISSORSNOKIA_HANDLECLIENT_H