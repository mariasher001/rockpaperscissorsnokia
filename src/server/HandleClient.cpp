//
// Created by maria on 7/7/2024.
//

#include "HandleClient.h"

std::mutex HandleClient::handleClientLogMutex;

void HandleClient::startCommunication(SOCKET socketConnectedToClient) {
    int clientId;
    std::string clientName;

    int clientErr = receiveClientDetails(socketConnectedToClient, clientId, clientName);
    if (clientErr != 0) {
        return;
    }

    {
        std::lock_guard<std::mutex> lock(handleClientLogMutex);
        std::cout << "[SERVER]: " << clientName << " connected!" << std::endl;
    }

    sendHighScore(socketConnectedToClient, clientId, clientName);
    startGame(socketConnectedToClient, clientId, clientName);
}

int HandleClient::receiveClientDetails(SOCKET socketConnectedToClient, int &clientId, std::string &clientName) {
    char receivedMsg[512];
    int msgLength = recv(socketConnectedToClient, receivedMsg, sizeof(receivedMsg), 0);
    if (msgLength > 0) {
        std::string clientDetails(receivedMsg, msgLength);
        int commaPosition = clientDetails.find(",");
        if (commaPosition != std::string::npos) {
            clientId = stoi(clientDetails.substr(0, commaPosition));
            clientName = clientDetails.substr(commaPosition + 1);
            return 0;
        } else {
            std::cerr << "[SERVER]: " << "wrong client name or id!" << std::endl;
            closesocket(socketConnectedToClient);
        }

    } else if (msgLength == 0) {
        std::cerr << "[SERVER]: " << "Client Socket closing!" << std::endl;
        closesocket(
                socketConnectedToClient); //Socket connected to this client will be closed! others will remain operational!
    } else {
        std::cerr << "[SERVER]: " << "Receive Failed!" << std::endl;
        closesocket(socketConnectedToClient);
    }
    return -1;
}

void HandleClient::sendHighScore(SOCKET socketConnectedToClient, int &clientId, std::string &clientName) {
    int highScore = HighScoreManager::getHighScore(clientName);
    std::string sendScore = std::to_string(highScore); //Converting int-highScore to string:sendScore
    send(socketConnectedToClient, sendScore.c_str(), sendScore.length(), 0);
}

void HandleClient::startGame(SOCKET socketConnectedToClient, int &clientId, std::string &clientName) {
    char receivedChoice[512];
    RockPaperScissorsEnum serverChoice;
    std::string result;
    while (true) {
        serverChoice = GameUtil::getRandomChoice();

        int receivedChoiceLength = recv(socketConnectedToClient, receivedChoice, sizeof(receivedChoice), 0);
        if (receivedChoiceLength > 0) {
            std::string clientChoice_str(receivedChoice, receivedChoiceLength);
            auto dollarPos = clientChoice_str.find('$');   //if $ is found => client sent score to update
            if (dollarPos != std::string::npos) {
                int score = stoi(clientChoice_str.substr(dollarPos + 1));
                HighScoreManager::updateHighScore(clientName, score);
            } else {                                        //no $ found => ROCK/PAPER/SCISSORS sent by client
                RockPaperScissorsEnum clientChoice = GameUtil::stringToEnum(clientChoice_str);

                result = GameUtil::getResult(clientChoice, serverChoice);

                {
                    std::lock_guard<std::mutex> lock(handleClientLogMutex);
                    std::cout << "[SERVER]: " << clientName << "-" << GameUtil::enumToString(clientChoice) << " server-"
                              << GameUtil::enumToString(serverChoice) << " Result: " << result << std::endl;
                }

                send(socketConnectedToClient, result.c_str(), result.length(), 0);

                if (result == "LOSE") {
                    std::lock_guard<std::mutex> lock(handleClientLogMutex);
                    HighScoreManager::displayTop10HighScores();
                    break;
                }
            }
        } else if (receivedChoiceLength == 0) {
            std::cerr << "[SERVER]: " << "Client Socket closing!" << std::endl;
            closesocket(socketConnectedToClient);
            break;
        } else {
            std::cerr << "[SERVER]: " << "Receive Failed!" << std::endl;
            closesocket(socketConnectedToClient);
            break;
        }
    }

}
