//
// Created by maria on 7/7/2024.
//

#include "HighScoreManager.h"

std::string HighScoreManager::filePath;
std::map<std::string, int> HighScoreManager::highScoreMap;
std::mutex HighScoreManager::highScoreMutex;

void HighScoreManager::loadHighScoresFromFile() {

    std::lock_guard<std::mutex> highScoreLock(highScoreMutex);

    filePath = "..\\HighScores.txt";
    std::ifstream inputFile;
    inputFile.open(filePath);
    if (!inputFile.is_open()) {
        std::cerr << "[SERVER]: " << "Failed to open the input file!" << std::endl;
        return;
    }

    highScoreMap.clear();

    std::string line;
    while (std::getline(inputFile, line)) {
        auto commaPos = line.find(',');
        if (commaPos != std::string::npos) {
            std::string clientName = line.substr(0, commaPos);
            int highScore = stoi(line.substr(commaPos + 1));
            highScoreMap[clientName] = highScore;
        }
    }

    inputFile.close();
}

void HighScoreManager::updateHighScore(std::string clientName, int score) {
    std::lock_guard<std::mutex> highScoreLock(highScoreMutex);

    if (highScoreMap.find(clientName) == highScoreMap.end() || highScoreMap[clientName] < score) {
        highScoreMap[clientName] = score;
        saveToFile();
    }
}

void HighScoreManager::saveToFile() {
    std::ofstream outputFile;
    outputFile.open(filePath);
    if (!outputFile.is_open()) {
        std::cerr << "[SERVER]: " << "Failed to open the output file!" << std::endl;
        return;
    }
    for (auto entry: highScoreMap) {
        outputFile << entry.first << "," << entry.second << std::endl;
    }
    outputFile.close();
}

int HighScoreManager::getHighScore(std::string &clientName) {
    std::lock_guard<std::mutex> highScoreLock(highScoreMutex);
    if (highScoreMap.find(clientName) != highScoreMap.end()) {
        return highScoreMap[clientName];
    }
    return 0;
}

void HighScoreManager::displayTop10HighScores() {
    std::lock_guard<std::mutex> highScoreLock(highScoreMutex);
    std::vector<std::pair<std::string, int>> highScoreVector(highScoreMap.begin(), highScoreMap.end());

    std::sort(highScoreVector.begin(), highScoreVector.end(), [](auto &a, auto &b) {
        return a.second > b.second;
    });

    std::string spaces = "                                                                 ";
    std::string border = "*****************************";
    std::cout << spaces + border << std::endl;
    std::cout << spaces + "TOP 10 HIGH SCORES" << std::endl;
    std::cout << spaces + border << std::endl;
    for (int i = 0; i < highScoreVector.size() && i < 10; ++i) {
        std::cout << spaces << (i + 1) << ". " << highScoreVector[i].first << " - " << highScoreVector[i].second << std::endl;
    }
    std::cout << spaces + border << std::endl;
}
