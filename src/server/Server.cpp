//
// Created by maria on 7/5/2024.
//

#include "Server.h"

Server::Server(char *ip, int port) : port(port), ip(ip) {}

void Server::startServer() {
    SocketUtil::createSocket(listenerSocket);
    SocketUtil::createIpPortService(ipPortService, ip, port);
    bindSocketToIpPortService();
    listenOnSocket();
    acceptConnection();
}

void Server::bindSocketToIpPortService() {
    int bindStatus = bind(listenerSocket, reinterpret_cast <const sockaddr *>(&ipPortService), sizeof(ipPortService));
    if (bindStatus == SOCKET_ERROR) {
        std::cerr << "[SERVER]: " << "Invalid Socket Binding" << WSAGetLastError() << std::endl;
        WSACleanup();
        exit(1);
    }
}

void Server::listenOnSocket() {
    int listenStatus = listen(listenerSocket, SOMAXCONN);
    if (listenStatus == SOCKET_ERROR) {
        std::cerr << "[SERVER]: " << "Error listening on port: " << port << WSAGetLastError() << std::endl;
        closesocket(listenerSocket);
        WSACleanup();
        exit(1);
    }

    std::lock_guard<std::mutex> lock(serverLogMutex);
    std::cout << "[SERVER]: " << "Listening on port " << this->port << std::endl;
}

void Server::acceptConnection() {
    while (true) {
        SOCKET socketConnectedToClient = accept(listenerSocket, nullptr, nullptr);
        if (socketConnectedToClient == INVALID_SOCKET) {
            std::cerr << "[SERVER]: " << " Invalid Socket: Accept failed" << WSAGetLastError() << std::endl;
        }

        std::thread newClientThread(&HandleClient::startCommunication, socketConnectedToClient);
        newClientThread.detach();
    }

}