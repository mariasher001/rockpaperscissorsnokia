//
// Created by maria on 7/7/2024.
//
#ifndef ROCKPAPERSCISSORSNOKIA_HIGHSCOREMANAGER_H
#define ROCKPAPERSCISSORSNOKIA_HIGHSCOREMANAGER_H

#include <string>
#include <map>
#include <vector>
#include <mutex>
#include <fstream>
#include <iostream>
#include <algorithm>

class HighScoreManager {
private:
    static std::string filePath;
    static std::map<std::string, int> highScoreMap;
    static std::mutex highScoreMutex;
public:
    static void loadHighScoresFromFile();
    static void updateHighScore(std::string clientName,int score);
    static void saveToFile();
    static int getHighScore(std::string &clientName);
    static void displayTop10HighScores();
};

#endif //ROCKPAPERSCISSORSNOKIA_HIGHSCOREMANAGER_H