//
// Created by maria on 7/7/2024.
//

#include "GameUtil.h"

std::string GameUtil::enumToString(RockPaperScissorsEnum e) {
    switch (e) {
        case RockPaperScissorsEnum::ROCK :
            return "ROCK";
        case RockPaperScissorsEnum::PAPER:
            return "PAPER";
        case RockPaperScissorsEnum::SCISSORS:
            return "SCISSORS";
        default:
            return "UNKNOWN";
    }
}

RockPaperScissorsEnum GameUtil::stringToEnum(std::string s) {
    if (s == "ROCK") return RockPaperScissorsEnum::ROCK;
    else if (s == "PAPER") return RockPaperScissorsEnum::PAPER;
    else return RockPaperScissorsEnum::SCISSORS;
}

RockPaperScissorsEnum GameUtil::getRandomChoice() {
    std::random_device random;
    std::mt19937 generator(random());
    std::uniform_int_distribution<> distribution(0, 2);
    return static_cast<RockPaperScissorsEnum>(distribution(generator));
}

std::string GameUtil::getResult(RockPaperScissorsEnum choice1, RockPaperScissorsEnum choice2) {
    if (choice1 == choice2)
        return "DRAW";
    if ((choice1 == RockPaperScissorsEnum::ROCK && choice2 == RockPaperScissorsEnum::SCISSORS) ||
        (choice1 == RockPaperScissorsEnum::SCISSORS && choice2 == RockPaperScissorsEnum::PAPER) ||
        (choice1 == RockPaperScissorsEnum::PAPER && choice2 == RockPaperScissorsEnum::ROCK)) {
        return "WIN";
    }
    return "LOSE";
}
