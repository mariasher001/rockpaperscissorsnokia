//
// Created by maria on 7/5/2024.
//

#ifndef ROCKPAPERSCISSORSNOKIA_SOCKETUTIL_H
#define ROCKPAPERSCISSORSNOKIA_SOCKETUTIL_H

#include <winsock2.h>
#include <iostream>
#include <ws2tcpip.h>

class SocketUtil{
public:
    static void initializeWinSocket();
    static void createSocket(SOCKET &Sock);
    static void createIpPortService(sockaddr_in &service,char* ip, int port);
};

#endif //ROCKPAPERSCISSORSNOKIA_SOCKETUTIL_H