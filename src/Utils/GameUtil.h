//
// Created by maria on 7/7/2024.
//

#ifndef ROCKPAPERSCISSORSNOKIA_GAMEUTIL_H
#define ROCKPAPERSCISSORSNOKIA_GAMEUTIL_H

#include <string>
#include <random>

enum class RockPaperScissorsEnum {
    ROCK=0, PAPER=1, SCISSORS=2
};

// This class contains an Utilities related to Game and RockPaperScissorsEnum
class GameUtil {
public:
    static RockPaperScissorsEnum getRandomChoice();
    static std::string enumToString(RockPaperScissorsEnum e);
    static RockPaperScissorsEnum stringToEnum(std::string s);
    static std::string getResult(RockPaperScissorsEnum choice1, RockPaperScissorsEnum choice2);
};


#endif //ROCKPAPERSCISSORSNOKIA_GAMEUTIL_H
