//
// Created by maria on 7/5/2024.
//

#include "SocketUtil.h"

void SocketUtil::initializeWinSocket() {
    WSAData wsaData;
    int wsaStatusCode = WSAStartup(2, &wsaData);
    if (wsaStatusCode != 0) {
        std::cerr << "winSock.dll not found!!" << std::endl;
        exit(1);
    }
}

void SocketUtil::createSocket(SOCKET &sock) {
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock == INVALID_SOCKET) {
        std::cerr << "Invalid Socket" << WSAGetLastError() << std::endl;
        WSACleanup();
        exit(1);
    }
}

void SocketUtil::createIpPortService(sockaddr_in &service, char *ip, int port) {
    service.sin_family = AF_INET;       //declare the family of service
    InetPton(AF_INET, ip, &service.sin_addr);   //setting Inet address
    service.sin_port = htons(port);     //Setting the port
}