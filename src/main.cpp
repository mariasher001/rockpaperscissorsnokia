#include <thread>
#include "server/Server.h"
#include "client/Client.h"
#include "server/HighScoreManager.h"

void createClients(char *serverIp, int serverPort, int numberOfClients);

void executeClient(char *serverIp, int serverPort, int clientId, std::string clientName);

/*
 * Checkout the README.md
 *
 * Lesser comments to see in the code because clean code states the method's name should define its function :)
 */
int main() {
    char serverIp[] = "127.0.0.1";
    int serverPort = 8080;
    int numberOfClients = 1;                    //Change the number of clients here

    SocketUtil::initializeWinSocket();
    HighScoreManager::loadHighScoresFromFile();

    createClients(serverIp, serverPort, numberOfClients);

    Server server(serverIp, serverPort); //Server runs on main thread initially
    server.startServer();

    return 0;
}

void createClients(char *serverIp, int serverPort, int numberOfClients) {
    for (int i = 0; i < numberOfClients; ++i) {
        std::string clientName = "Client" + std::to_string(i);
        std::thread clientThread(&executeClient, serverIp, serverPort, i, clientName);      //New Thread for each client
        clientThread.detach();
    }
}

void executeClient(char *serverIp, int serverPort, int clientId, std::string clientName) {
    Client client(serverIp, serverPort, clientId, clientName);
    client.startClient();
}

