//
// Created by maria on 7/7/2024.
//
#ifndef ROCKPAPERSCISSORSNOKIA_COMMUNICATION_H
#define ROCKPAPERSCISSORSNOKIA_COMMUNICATION_H


#include <string>
#include <winsock2.h>
#include <mutex>
#include <iostream>
#include "../Utils/GameUtil.h"

class Communication {
private:
    SOCKET clientSocket;
    int clientId;
    std::string clientName;
    std::mutex communicationLogMutex;
    void sendClientDetails();
    int receiveHighScore();
    void startGame();

public:
    Communication(SOCKET clientSocket, int clientId, std::string clientName);
    void startCommunication();
};

#endif //ROCKPAPERSCISSORSNOKIA_COMMUNICATION_H