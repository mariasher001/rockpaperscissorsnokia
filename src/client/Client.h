//
// Created by maria on 7/7/2024.
//

#ifndef ROCKPAPERSCISSORSNOKIA_CLIENT_H
#define ROCKPAPERSCISSORSNOKIA_CLIENT_H

#include <winsock2.h>
#include <iostream>
#include "../Utils/SocketUtil.h"
#include "Communication.h"

class Client {
private:
    char *serverIp;
    int serverPort;
    int clientId;
    std::string clientName;
    SOCKET clientSocket;
    sockaddr_in remoteIpPortService;
    void connectToServerIpPortService();

public:
    Client(char *serverIp, int serverPort, int clientId, std::string &clientName);
    void startClient();

};

#endif //ROCKPAPERSCISSORSNOKIA_CLIENT_H