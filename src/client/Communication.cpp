//
// Created by maria on 7/7/2024.
//

#include "Communication.h"

Communication::Communication(SOCKET clientSocket, int clientId, std::string clientName) {
    this->clientSocket = clientSocket;
    this->clientId = clientId;
    this->clientName = clientName;
}

void Communication::startCommunication() {
    sendClientDetails();
    int isReceived = receiveHighScore();
    if (isReceived != 0)
        return;

    startGame();
}

void Communication::sendClientDetails() {
    std::string clientDetail = std::to_string(clientId) + "," + clientName;
    send(clientSocket, clientDetail.c_str(), clientDetail.length(), 0);
}

int Communication::receiveHighScore() {
    char receivedScore[512];
    int scoreMsgLen = recv(clientSocket, receivedScore, sizeof(receivedScore), 0);
    if (scoreMsgLen > 0) {
        std::lock_guard<std::mutex> lock(communicationLogMutex);

        std::string receivedScore_Str(receivedScore, scoreMsgLen);
        std::cout << "[CLIENT]: " << clientName << " - Last HighScore : " << receivedScore_Str << std::endl;
        return 0;
    } else if (scoreMsgLen == 0) {
        std::cerr << "[CLIENT]: " << "Client Socket closing!" << std::endl;
        closesocket(clientSocket);
    } else {
        std::cerr << "[CLIENT]: " << "Receive Failed!" << std::endl;
        closesocket(clientSocket);
    }
    return -1;
}

void Communication::startGame() {
    RockPaperScissorsEnum choice;
    char receivedResult[512];
    int score = 0;
    while (true) {
        choice = GameUtil::getRandomChoice();
        std::string choice_str = GameUtil::enumToString(choice);

        send(clientSocket, choice_str.c_str(), choice_str.length(), 0);

        int receivedResultLength = recv(clientSocket, receivedResult, sizeof(receivedResult), 0);
        if (receivedResultLength > 0) {
            std::string result_str(receivedResult, receivedResultLength);
            if (result_str == "WIN") {
                std::string scoreUpdate = "$" + std::to_string(++score);
                send(clientSocket, scoreUpdate.c_str(), scoreUpdate.length(), 0);
            } else if (result_str == "LOSE") {
                std::lock_guard<std::mutex> lock(communicationLogMutex);
                std::cout << "[CLIENT]: " << clientName + " GAME FINISHED! score = " << score << std::endl;
                break;
            }
        } else if (receivedResultLength == 0) {
            std::cerr << "[CLIENT]: " << "Client Socket closing!" << std::endl;
            closesocket(clientSocket);
            break;
        } else {
            std::cerr << "[CLIENT]: " << "Receive Failed!" << std::endl;
            closesocket(clientSocket);
            break;
        }
    }
}