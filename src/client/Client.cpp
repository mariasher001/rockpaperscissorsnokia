//
// Created by maria on 7/7/2024.
//

#include "Client.h"

Client::Client(char *serverIp, int serverPort, int clientId, std::string &clientName) {
    this->serverIp = serverIp;
    this->serverPort = serverPort;
    this->clientId = clientId;
    this->clientName = clientName;
}

void Client::startClient() {
    SocketUtil::createSocket(clientSocket);
    SocketUtil::createIpPortService(remoteIpPortService, serverIp, serverPort);
    connectToServerIpPortService();

}

void Client::connectToServerIpPortService() {
    int isConnected = connect(clientSocket, reinterpret_cast <const sockaddr *>(&remoteIpPortService),
                              sizeof(remoteIpPortService));
    if (isConnected == SOCKET_ERROR) {
        std::cerr << "[CLIENT]: " << " Connection to Server failed!" << std::endl;
        WSACleanup();
        exit(1);
    }
    Communication communication(clientSocket, clientId, clientName);
    communication.startCommunication();
}
